#! usr/bin/env python3

"""This is a program that
"""

__author__ = 'Susan Reefman'

# imports
import sys
from Bio import SeqIO
from Bio import pairwise2


def get_lists(args):
    """" Reading the input file
    Saves the sequence record id's and the sequence record sequences in separate lists """
    seq_records = []
    sequences = []
    # Reading input file
    for seq_record in SeqIO.parse(args, "fasta"):
        seq_records.append(seq_record.id)
        sequences.append(seq_record.seq)

    return sequences, seq_records

def get_aligment(sequences, seq_records):
    """" Returns print statements with the sequence records id's
     and the grade the alignment gets """
    base = 0
    count = 1
    while count < len(sequences):
        # Aligning a base sequence with another sequence
        alignments = pairwise2.align.globalxx(sequences[base], sequences[count])
        # Getting the score of the aligment
        for alignment in alignments:
            score = alignment[2]
        length = len(sequences[count])
        # Getting a grade with the score
        getal = get_scores(score, length)
        print('base: %s, compared with: %s' % (seq_records[base],seq_records[count]))
        print('Score = %d' % (getal))
        count += 1

def get_scores(score, length):
    """" Returns matching grade of the score of the alignment
     Calculates the grade with the score and the length given"""
    if score < length * 0.6:
        grade = 10
        return grade
    if score < length * 0.70:
        grade = 9
        return grade
    if score < length * 0.75:
        grade = 8
        return grade
    if score < length * 0.80:
        grade = 7
        return grade
    if score < length * 0.85:
        grade = 6
        return grade
    if score < length * 0.90:
        grade = 5
        return grade
    if score < length * 0.93:
        grade = 4
        return grade
    if score < length * 0.95:
        grade = 3
        return grade
    if score < length * 0.97:
        grade = 2
        return grade
    if score > length * 0.99:
        grade = 1
        return grade

def main(args):
    if len(args) < 2:
        print("Give a file as input (python snp.py file.fasta)")
    else:
        sequences, seq_records = get_lists(args[1])
        get_aligment(sequences, seq_records)

    return 0

if __name__ == '__main__':
    EXITCODE = main(sys.argv)
    sys.exit(EXITCODE)
