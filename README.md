## Opdrachten Bio-Informatica 3

- Author: Susan Reefman 
- Date: 5-11-2021

---

### Opdracht 1

Text file with answer to question 1

---

### Opdracht 2

Python file (Opdracht2.py) with program and fasta file (homologene.fasta) with data to test program.
Run program: 
python Opdracht2.py homologene.fasta